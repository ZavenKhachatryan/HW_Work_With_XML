﻿using System.Xml;

namespace HW_WorkWithXML
{
    class Person<T>
    {
        public string name;
        public string surname;
        public int age;
        public string email;
        public string phone;

        public void WriteStudents(XmlTextWriter xmlWrite , string specialty)
        {
            xmlWrite.WriteStartElement(specialty);

            xmlWrite.WriteStartElement("Name");
            xmlWrite.WriteString(name);
            xmlWrite.WriteEndElement();

            xmlWrite.WriteStartElement("Surname");
            xmlWrite.WriteString(surname);
            xmlWrite.WriteEndElement();

            xmlWrite.WriteStartElement("Age");
            string strAge = age.ToString();
            xmlWrite.WriteString(strAge);
            xmlWrite.WriteEndElement();

            xmlWrite.WriteStartElement("Email");
            xmlWrite.WriteString(email);
            xmlWrite.WriteEndElement();

            xmlWrite.WriteStartElement("Phone");
            xmlWrite.WriteString(phone);
            xmlWrite.WriteEndElement();

            xmlWrite.WriteEndElement();
        }

    }
}

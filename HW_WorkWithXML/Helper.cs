﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;

namespace HW_WorkWithXML
{
    class Helper
    {
        public static void SaveToXmlFile<T>(string path, IEnumerable<Person<T>> persons)
        {
            var xmlWrite = new XmlTextWriter(path, null)
            {
                Formatting = Formatting.Indented,
                IndentChar = ' ',
                Indentation = 2,
            };

            string speciality = path.Substring(0, path.Length - 5);

            xmlWrite.WriteStartDocument();
            xmlWrite.WriteStartElement($"ListOf{speciality}s");

            foreach (Person<T> person in persons)
                person.WriteStudents(xmlWrite, speciality);

            xmlWrite.WriteEndElement();
            xmlWrite.Close();
            Console.WriteLine("Your .xml file is created !!!\n\n");
        }

        public static void LoadAllInformationForPersons(string path)
        {
            var personDoc = new XmlDocument();
            personDoc.Load(path);

            XmlNode root = personDoc.DocumentElement;

            foreach (XmlNode perosons in root.ChildNodes)
            {
                foreach (XmlNode person in perosons.ChildNodes)
                    Console.WriteLine($"{person.Name} : {person.InnerText}");
                Console.WriteLine(new string('*', 40));
            }
        }

        public static void LoadFromFieldOfPersons(string path, string fieldName)
        {
            var personsDoc = new XPathDocument(path);
            XPathNavigator navigator = personsDoc.CreateNavigator();
            fieldName = fieldName.ToLower();
            fieldName = fieldName.Substring(0, 1).ToUpper() + (fieldName.Length > 1 ? fieldName.Substring(1) : "");

            string speciality = path.Substring(0, path.Length - 5);
            XPathExpression expr = navigator.Compile($"ListOf{speciality}s/{speciality}/{fieldName}");
            XPathNodeIterator iterator = navigator.Select(expr);

            foreach (var item in iterator)
            {
                Console.WriteLine($"\n{new string('*', 40)}");
                Console.WriteLine($"{fieldName} : {item}");
            }
            Console.WriteLine($"\n{new string('*', 40)}");
        }
    }
}

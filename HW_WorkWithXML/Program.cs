﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HW_WorkWithXML
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = CreateStudents(200).ToList();
            Helper.SaveToXmlFile("Students.xml", students);
            Helper.LoadAllInformationForPersons("Students.xml");
            Helper.LoadFromFieldOfPersons("Students.xml", "name");

            Console.ReadLine();
        }

        static IEnumerable<Student> CreateStudents(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var st = new Student()
                {
                    name = $"A{i}",
                    surname = $"A{i}yan",
                    age = rnd.Next(18, 45),
                    email = $"A{i}A{i}yan@gmail.com",
                    phone = $"+374 09{rnd.Next(2, 9)} {rnd.Next(100, 999)} {rnd.Next(100, 999)}"
                };
                yield return st;
            }
        }
    }
}
